<?php
/**
 * stationfive functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package stationfive
 * @version 1.0
 */

/* Add styles and script*/
function stationfive_enqueue_styles() {
    wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css' );

    wp_enqueue_script( 'bootstrap-script', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'number-script', get_stylesheet_directory_uri() . '/js/numbercounter.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), '1.0.0', true );
    
}
add_action( 'wp_enqueue_scripts', 'stationfive_enqueue_styles' );

/* Save donation data to DB */
if( isset($_POST['btndonate']) ) {
    // Get post id with post_type = 'donations'
    $args = array( 
        'post_status' => 'publish',
        'post_type'   => 'donations'
    );
    $donation = new WP_Query( $args );

    $data = array(
        'firstname'        => $_POST['firstname'],
        'lastname'         => $_POST['lastname'],
        'email'            => $_POST['email'],
        'phone'            => $_POST['phone'],
        'donation_amount'  => $_POST['donation-amount'],
        'payment_method'   => $_POST['paymentMethod'],
    );

    if( $donation->have_posts() ) {
        while( $donation->have_posts() ) {
            $donation->the_post();
            $post_id = get_the_ID();
            
            // insert donation metadata
            foreach( $data as $key => $value ) {
                add_post_meta( $post_id, $key, $value );
            }

        echo "<script>alert('Thank you for the donation.')</script>";
        }
        
    } else {
        $post = array(
            'post_content'  =>  'Donation Content',
            'post_title'    =>  'Donations',
            'post_status'   =>  'publish',
            'post_type'     =>  'donations'
        );
        $post_idx = wp_insert_post( $post, true );

        if( is_wp_error($post_id) ) {
            // Error handling
            echo "<script>console.log('Error saving data.')</script>";
        } else {
            // insert metadata
            foreach( $data as $key => $value ) {
                update_post_meta( $post_idx, $key, $value );
            }
            
            echo "<script>alert('Thank you for the donation')</script>";
        }
    }

    
}

?>