<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package stationfive
 * @version 1.0
 */

get_header(); ?>
<div class="donation-section">
    <div class="container mx-auto">
        
        <div class="p-5">
            <h1 class="text-center text-white">Lorem Ipsum</h1>
            <p class="text-center px-5 pb-2 text-white">
                Lorem ipsum dolor sit amet, consetetur sadispscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
            </p>
            <div id="donation-form" class="card shadow bg-body border-0">
                <div class="card-body p-5">
                <form action="#" role="form" method="post">
                    <div class="current-donation-wrapper text-center">
                        <?php 
                            // Get post id with post_type = 'donations'
                            $args = array( 
                                'post_status' => 'publish',
                                'post_type'   => 'donations'
                            );
                            $donation = new WP_Query( $args );
                            if( $donation->have_posts() ) {
                                while( $donation->have_posts() ) {
                                    $donation->the_post();
                                    $post_id = get_the_ID();
                                    
                                    // get donation metadata
                                    $donations = get_post_meta( $post_id, 'donation_amount' );
                                    
                        
                                echo "<script>console.log('". array_sum($donations) ."')</script>";
                                }
                                $amount = array_sum($donations);
                            } else {
                                $amount = 0;
                                echo "<script>console.log('No data retrieved.')</script>";
                            }
                            
                        ?>
                        <h5>Lorem ipsum</h5>
                        <h1 class="current-donation-amount text-blue-green timer count-number" data-to="<?= $amount ?>" data-speed="1500"></h1>
                        <h5 class="donation-raised" style="font-weight: 400;" data-to="4000000">of $4 million raised</h5>

                        <div class="progress-wrapper mx-auto mt-4 mb-4">
                            <!-- Progress bar HTML -->
                            <div class="progress">
                                <div class="progress-bar bg-blue-green" style="min-width: 0px;"></div>
                            </div>

                        </div>

                    </div>
                    <div class="input-group mb-3 mx-auto input-amount">
                        <span class="input-group-text">$</span>
                        <input type="text" id="donation-amount" name="donation-amount" class="form-control" required>
                    </div>
                    
                    <p class="mt-3 mb-3 mx-auto text-black-50 ">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elit, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aluquyan erat, sed diam volupta. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita
                    </p>
                    <div class="payment-info-wrapper">
                        <h5 class="pt-3 pb-3">Select payment method</h5>
                        <div class="form-check form-check-inline" style="padding-left: 0;">
                            <input type="radio" name="paymentMethod" id="paypal" value="paypal" checked>
                            <label for="paypal">Paypal</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" name="paymentMethod" id="offlineDonation" value="offline-donation">
                            <label for="offlineDonation">Offline donation</label>
                        </div>

                        <h5 class="pt-3 pb-3">Personal info</h5>
                        <div class="row pb-3">
                            <div class="col-sm-6 pb-1">
                                <input class="form-control" type="text" name="firstname" placeholder="First name*" required>
                            </div>
                            <div class="col-sm-6 pb-1">
                                <input class="form-control" type="text" name="lastname" placeholder="Last name*" required>
                            </div>
                        </div>
                        <div class="row pb-3">
                            <div class="col-sm-6 pb-1">
                                <input class="form-control" type="email" name="email" placeholder="Email*" required>
                            </div>
                            <div class="col-sm-6 pb-1">
                                <input class="form-control" type="text" name="phone" placeholder="Phone*" required>
                            </div>
                        </div>
                        

                        <div class="input-group mb-3 mx-auto display-amount">
                            <span class="input-group-text">Donation total:</span>
                            <input type="text" id="donation-total" class="form-control" readonly>
                        </div>
                        <div class="text-center">
                            <input type="submit" name="btndonate" style="font-size: 13px; padding: 10px 40px;" class="btn bg-blue-green text-white" value="DONATE NOW">
                        </div>
                    </div>
                    </form>
                </div> <!-- end of .card-body -->
            </div>
        </div>
        
    </div>
</div>
<div class="testimonial-section container pt-5 pb-5">
<div class="row">
    <div class="col-sm-6">
     
    </div>
    <div class="col-sm-6 mx-auto px-7">
        <div class="vertical-center">
            <img class="quote-right" src="<?php echo get_template_directory_uri(); ?>/img/symbol-L.svg">
            <h6>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</h6>
            <p class="pt-3 text-black-50">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea</p>
            <img class="quote-left" src="<?php echo get_template_directory_uri(); ?>/img/symbol-L.svg" alt="">
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>  