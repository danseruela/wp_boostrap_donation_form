<?php
/**
 * The template for displaying the footer
 * 
 * @package stationfive
 * @version 1.0
 */
?>

<footer class="blog-footer">
  <div class="container">
    <div class="row mactanbank-footer">
      <?php get_template_part( 'template-parts/footer/footer', 'widgets' ) ?>
    </div>
  </div>  
      <?php get_template_part( 'template-parts/footer/site', 'info' ) ?>
</footer>


<?php wp_footer(); ?>
    
</body>
</html>