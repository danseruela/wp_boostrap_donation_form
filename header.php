<?php
 /**
  * The template for displaying the header
  * 
  * @package stationfive
  * @version 1.0
  */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>
      <?php 
          if (function_exists('is_tag') && is_tag()) { 
            echo 'Tag Archive for &quot;'.$tag.'&quot; - '; 
          } elseif (is_archive()) { 
            wp_title(''); echo ' Archive - '; 
          } elseif (is_search()) { 
            echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; 
          } elseif (is_home() || is_front_page()) {
            bloginfo('name'); 
          } elseif (!(is_404()) && (is_single()) || (is_page())) { 
            wp_title('|', true, 'right'); //echo ' - '; 
          } elseif (is_404()) {
            echo 'Not Found - '; 
          } else {
            bloginfo('name'); 
          }
      ?>
    </title>

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
