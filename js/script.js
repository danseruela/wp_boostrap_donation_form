jQuery(document).ready(function($) {

    // progress bar
    var donationAmount = $(".current-donation-amount").data("to");
    var raisedAmount= $(".donation-raised").data("to");
    var totalPercentage = (donationAmount / raisedAmount) * 100;

    console.log("Donation: " + donationAmount);
    console.log("Raised Amount: " + raisedAmount);
    console.log("Total Percentage: " + totalPercentage);

    var i = 0;
    function makeProgress(){
        if(i < totalPercentage){
            i = i + 1;
            $(".progress-bar").css("width", i + "%");
        }
        // Wait for sometime before running this script again
        setTimeout(makeProgress, 100);
    }

    makeProgress();

    // Display donation total amount
    $("#donation-amount").keyup("blur", function() {
        $("#donation-total").val( $("#donation-amount").val() );
    });
});

